package com.beantest.freebuilder;

import com.beantest.domain.Seat;
import org.inferred.freebuilder.FreeBuilder;

/**
 */
@FreeBuilder
public abstract class FBSeat implements Seat {

    @Override
    public abstract String getPassengerName();

    public static class Builder extends FBSeat_Builder { }
}
