package com.beantest.lombok;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class LombokTest {

    @Test
    public void testBuilder() {
        assertThat(LombokBus.builder().driverName("John").brand("Ford").name("schoolbus").build()).isNotNull();
        assertThatThrownBy(() -> LombokBus.builder().name(null).build()).isInstanceOf(NullPointerException.class);
    }
}
