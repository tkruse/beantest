# beantest

compare bean annotation utilities

Dependencies
```
+--- org.mapstruct:mapstruct-processor:1.2.0.Beta3
|
+--- org.inferred:freebuilder:1.14.1
+--- com.google.auto.value:auto-value:1.4.1
+--- org.projectlombok:lombok:1.16.18
+--- org.immutables:value:2.5.6
+--- org.immutables:builder:2.5.6
\--- org.immutables:gson:2.5.6
     \--- com.google.code.gson:gson:2.8.0

runtime - Runtime dependencies for source set 'main' (deprecated, use 'runtimeOnly ' instead).
+--- org.mapstruct:mapstruct-jdk8:1.2.0.Beta3
```

## Annotation Processing

Maven + IntelliJ: OK, no changes required

Setting up Gradle and IntelliJ using the IntelliJ gradle integration
in a consistent manner was nearly impossible, due to IntelliJ bugs.
Only after a bug report and an update it was finally possible to
use gradle with the org.inferred.processors gradle plugin. This
seems likely to happen again with new IntelliJ versions because
the behavior seems to change between versions arbitrarily.

The ideal combination seems quite impossible to achieve:
- dirty build pass in IntelliJ
- dirty build pass in gradle
- java files are generated outside build/classes
- gradle and intelliJ generate java files into the same folder
- all build artifacts are inside the build folder
- IntelliJ gradle integration marks the folder of generated java files as src folder

The gradle net.ltgt.apt plugin has surprising behavior.
When adding one apt dependency, all other annotationprocessors
become unused unless they are also made an apt dependency.
The plugin claims to clean up folder handling, but in my case it made things worse.

### Nullness checking

Verifying accesses via Findbugs is tricky. It get's even more tricky to check what
happens when using gradle, because gradle caches .class files, and that caching strategy
is buggy with respect to changing annotations (I got inconsistent results, but
so far hard to reproduce for a bug report).

The different builder generation frameworks struggle with consistently
handling the Findbugs annotations in a way that satisfies Findbugs.
Findbugs itself easily seems to get confused over the file structures.

### Immutability

Immutability can be nice in programming for multi-threading and caching.
In Java, Mutability is difficult to guarantee, because the final keyword
does not protect from fields being mutable instances.

### Usability

Using Code Generators takes a while to adapt to, in particular when mixing multiple annotators.
The build errors after refactorings can be confusing, any failure in the chain of generators
may produce irrelevant consecutive errors.

## Bean Creation Facilitators

### Lombok 1.18.22

Lombok is the only tool that does not generate Java source code.
It is easiest on the eye in the code. E.g. it allows to provide
special method to distinguish setSingleFoo(foo), setFoos(fooList).

The drawback is that it uses a hack of the compiler to work,
so future compiler version might not support it.
Also it only generates mutable beans.

With Java9, running lombok will become much more difficult, and might
seriously impact lombok usage (too early to say).

* IntelliJ: Requires enable annotations, requires lombok plugin
* Debugging: Not inside Builder
* Inheritence: Requires Builder annotation on special constructor
* Findbugs: ok
* MapStruct: ok


### AutoValue 1.4.1

AutoValue offers the least bells and whistles, it seems
it targets Android trying to avoid generation of many methods.

* requires abstract class, inner abstract class, builder methods
* Generated beans are not mutable  (no setters)
* heuristically guesses programmer intentions for naming get/set
* requires @Nullable annotation to allow null values
* Builder possible, but make constructor private.
* Cannot extend other AutoValue classes (theoretical issues)
* Validation extra problematic with builder
* Default Values possible, but clunky
* Findbugs ok
* MapStruct: no solution, only ideas at this time
* Collection methods support (add...): Possible with extra manual code

### FreeBuilder 2.7.0

* requires abstract class or interface
* Generated beans are not mutable (no setters)
* List and Map have nice Builder methods to add instead of setting
* Can extend other FreeBuilder classes
* Constraints and defaults easy to add to builder
* Findbugs ok, does not pass on @CheckForNull, reported, maybe fixed soon
* Vulnerability due to floating point comparisons (autoValue and Immutables use safer pattern)
* MapStruct: no solution, only ideas at this time
* Support any "Nullable" annotation
* Creates OptionalInt, OptionalLOng, OptionalDouble accessors

### Immutables 2.5.6

* Requires abstract class or interface
* lists and maps have nice builder methods to add instead of setting
* Findbugs NOT ok, does not pass on @CheckForNull, or without it.
* Can extend other immutables
* Ensures that collection members are truly immutable (by wrapping)
* MapStruct: no solution, only ideas at this time


## Bean Mappers

Bean mappers appear and go out of maintenance regularly. Meaning the problem is recurring, teams think they can create a better solutthan reusing an existing mapper, but the effort to maintain is too high to be sustainable.
* Dozer (2006-2014)
* Selma (2014-2017)
* Doov (2018-2019)
* Orika (2012-2019)

The combination with the autogenerated Beans is complex. For Lombok, the order
of processors is crucial. For the other code generators as well, but more importantly
immutable classes do not have public constructors / setters, so the Bean Mappers
would need reflection or mapping to the Builders.

### MapStruct 1.4.2.Final 

1.2.0.Beta3: Now has lombok support. Mapping to AutoValue, Immutables and FreBuilder
does not currently work, some ideas exist on mapping to the Builders.

Lombok generally seems to be active in trying to quickly fix any incompatibility to mapstruct.

## More TODOs

* Debugging
* Default values
* Java 9
* Inheritence
* Null handling
* Handle Collections (adders, Singulars)
* Use with Jackson
* Use with Bean Validation
* Use with JPA
* Use with BeanMapper
* Use with random-beans
