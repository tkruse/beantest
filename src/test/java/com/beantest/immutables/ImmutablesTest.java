package com.beantest.immutables;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ImmutablesTest {

    @Test
    public void testBuilder() {
        assertThat(ImmutableImmuBus.builder().driverName("John").brand("Ford").name("schoolbus").build()).isNotNull();
        assertThatThrownBy(() -> ImmutableImmuBus.builder().name(null).build()).isInstanceOf(NullPointerException.class);
    }

}
