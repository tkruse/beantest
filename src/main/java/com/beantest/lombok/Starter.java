package com.beantest.lombok;


public class Starter {

    public static void main(final String[] args) {
        // OK: NPE in constructor
        // LombokBus bus1 = new LombokBus(null, null, null, null);
        // OK: NPE in constructor
        // LombokBus bus2 = LombokBus.builder().driverName("John").build();

        // OK: NPE in setName, Findbugs warning

        // OK: Findbug warnings about NullPointerDereference
        final LombokBus bus3 = LombokBus.builder().driverName("John").name("CityBus").build();
        bus3.setName(null);
        //System.out.println(bus3.getDriverName().length());
    }
}
