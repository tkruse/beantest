package com.beantest.freebuilder;


public class Starter {

    public static void main(final String[] args) {

        // Not OK: Findbug no warnings about NullPointerDereference
        // if generated java found by findbugs
        // final FBBus bus3 = new FBBus.Builder().setName("CityBus").build();
        //System.out.println(bus3.getDriverName().length());
    }
}
