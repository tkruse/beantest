/**
 * Versions for build tool dependencies
 */
object ToolVersions {
    const val checkstyle = "9.2.1"
    const val pmd = "6.41.0"
    const val spotbugs = "4.5.3"
    const val errorProne = "2.10.0"
}

object Versions {
    const val lombok             = "1.18.22"
    const val immutables         = "2.8.8"
    const val autovalue          = "1.9"
    const val freebuilder        = "2.7.0"
    const val javax_validation   = "2.0.1.Final"
    const val hibernate_valid    = "7.0.2.Final"
    /*** TESTING  ***/

    const val jackson = "2.13.1"
    const val guava = "31.0.1-jre"

    const val mapstruct = "1.4.2.Final"
    const val jsr305 = "3.0.2"

    const val logback = "1.2.10"
    const val slf4j = "1.7.32"

    const val junit = "4.13.2"
    const val mockito2 = "4.2.0"
    const val powermock = "2.0.9"
    const val hamcrest = "1.3"
    const val restassured = "3.0.7" // also for io.rest-assured:json-path
}


object Libraries {

    val jackson_core by lazy { "com.fasterxml.jackson.core:jackson-core:${Versions.jackson}" }
    val jackson_databind by lazy { "com.fasterxml.jackson.core:jackson-databind:${Versions.jackson}" }
    val jackson_annotations by lazy { "com.fasterxml.jackson.core:jackson-annotations:${Versions.jackson}" }

    val commons_lang3 by lazy { "org.apache.commons:commons-lang3:3.12.0" }
    val commons_coll4 by lazy { "org.apache.commons:commons-collections4:4.4" }
    val commons_io by lazy { "commons-io:commons-io:2.11.0" }
    val commons_codec by lazy { "commons-codec:commons-codec:1.15" }

    val guava by lazy { "com.google.guava:guava:${Versions.guava}" }

    val jsr305 by lazy { "com.google.code.findbugs:jsr305:${Versions.jsr305}" }

    val javax_inject by lazy { "javax.inject:javax.inject:1" }

    val slf4j_api by lazy { "org.slf4j:slf4j-api:${Versions.slf4j}" }
    val jcl_over_slf4j by lazy { "org.slf4j:jcl-over-slf4j:${Versions.slf4j}" }
    val log4j_over_slf4j by lazy { "org.slf4j:log4j-over-slf4j:${Versions.slf4j}" }
    val logback by lazy { "ch.qos.logback:logback-classic:${Versions.logback}" }

    val mapstruct by lazy { "org.mapstruct:mapstruct:${Versions.mapstruct}" }
    val mapstruct_processor by lazy { "org.mapstruct:mapstruct-processor:${Versions.mapstruct}" }

    // test dependencies
    val junit by lazy { "junit:junit" }
    val assertj_core by lazy { "org.assertj:assertj-core:3.22.0" }
    val mockito2 by lazy { "org.mockito:mockito-core:${Versions.mockito2}" }
    val hamcrest by lazy { "org.hamcrest:hamcrest-all:${Versions.hamcrest}" }
    val powermock by lazy { "org.powermock:powermock-module-junit4:${Versions.powermock}" }
    val powermock_mockito2 by lazy { "org.powermock:powermock-api-mockito2:${Versions.powermock}" }
    val awaitility by lazy { "org.awaitility:awaitility:4.1.1" }
    val wiremock by lazy { "com.github.tomakehurst:wiremock:2.15.0" }
    val rest_assured by lazy { "io.rest-assured:rest-assured:${Versions.restassured}" }
}
