import org.gradle.api.tasks.testing.logging.TestExceptionFormat


plugins {
    `java-library`
    id("java-dependencies-convention")
    id("checkstyle-convention")
    id("pmd-convention")
    id("spotbugs-convention")
    idea
    eclipse
    id("com.github.ben-manes.versions")
    id("org.inferred.processors")
    id("com.diffplug.spotless")
}


java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.check {
    mustRunAfter(tasks["clean"])
}


tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.addAll(arrayOf(
        "-Xlint:all",
        "-Xlint:-processing",
        // "-Xlint:-serial",
        // "-Xlint:-cast",
        "-Werror"
    ))
}

tasks.test {
    useJUnit()
    minHeapSize = "512m"
    testLogging {
        showStandardStreams = true
        exceptionFormat = TestExceptionFormat.FULL // default is "short"

    }
}


//spotless {
//    format 'misc', {
//    // define the files to apply `misc` to
//    target '*.gradle', '*.md', '.gitignore'
//
//    // define the steps to apply to those files
//    trimTrailingWhitespace()
//    indentWithTabs() // or spaces. Takes an integer argument if you don't like 4
//    endWithNewline()
//}
//    java {
//        googleJavaFormat('1.8').aosp().reflowLongStrings()
//    }
//}
