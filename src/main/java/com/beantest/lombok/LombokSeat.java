package com.beantest.lombok;

import com.beantest.domain.Seat;
import lombok.Data;

/**
 */
@Data
public class LombokSeat implements Seat {

    private String passengerName;

}
