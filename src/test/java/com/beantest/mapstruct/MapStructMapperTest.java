package com.beantest.mapstruct;

import com.beantest.autovalue.AVBus;
import com.beantest.freebuilder.FBBus;
import com.beantest.immutables.ImmuBus;
import com.beantest.immutables.ImmutableImmuBus;
import com.beantest.lombok.LombokBus;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class MapStructMapperTest {

    private static final MapStructMapper MAPPER = Mappers.getMapper(MapStructMapper.class);

    @Test
    public void testImmutableToLombok() {
        final ImmuBus imBus = ImmutableImmuBus.builder().driverName("John").brand("Ford").name("schoolbus").build();

        final LombokBus lombokBus = MAPPER.immutableToLombok(imBus);
        assertThat(lombokBus).usingRecursiveComparison().isEqualTo(imBus);
    }

    @Test
    public void testAutoValueToLombok() {
        final AVBus avBus = AVBus.builder().setDriverName("John").setBrand("Ford").setName("schoolbus").build();

        final LombokBus lombokBus = MAPPER.autoValueToLombok(avBus);
        assertThat(lombokBus).usingRecursiveComparison().isEqualTo(avBus);

    }

    @Test
    public void testFreeBuilderToLombok() {
        final FBBus fbBus = new FBBus.Builder().setDriverName("John").setBrand("Ford").setName("schoolbus").build();

        final LombokBus lombokBus = MAPPER.freebuilderToLombok(fbBus);
        assertThat(lombokBus).usingRecursiveComparison().isEqualTo(fbBus);

    }

}
