plugins {
    `java-library`
}

// Common dependencies for any java-library module in this project
dependencies {
    implementation(Libraries.commons_lang3)
    implementation(Libraries.commons_coll4)
    implementation(Libraries.guava)
    // required for guava :-(
    implementation("com.google.errorprone:error_prone_annotations") {
        version {
            prefer(ToolVersions.errorProne)
        }
    }

    implementation(Libraries.slf4j_api)
    implementation(Libraries.javax_inject)

    // provides javax.annotationsNonnull and CheckForNull, no runtime dependency
    compileOnly(Libraries.jsr305)
    testCompileOnly(Libraries.jsr305)

    runtimeOnly(Libraries.jcl_over_slf4j)
    runtimeOnly(Libraries.log4j_over_slf4j)
    runtimeOnly(Libraries.logback)

    testImplementation(Libraries.junit) {
        version {
            prefer(Versions.junit)
        }
    }
    testImplementation(Libraries.hamcrest)
    testImplementation(Libraries.assertj_core)
    testImplementation(Libraries.mockito2) {

    }
    testImplementation(Libraries.powermock) {
        // use mockito version2
        exclude(group = "net.bytebuddy")
        exclude(group = "org.objenesis")
    }
    testImplementation(Libraries.powermock_mockito2) {
        exclude(group = "org.mockito")
        // use mockito version2
        exclude(group = "net.bytebuddy")
        exclude(group = "org.objenesis")
    }
    testImplementation(Libraries.awaitility)
}

// restrict transitive dependencies for java projects. Play is too messy to do that.
configurations {
    // do not allow using transitive dependencies (declare what you use)
    compileClasspath {
        isTransitive = false
    }
    // allow transitive dependencies in tests (less risk for product)
    testImplementation {
        isTransitive = true
    }
    testCompileOnly {
        isTransitive = true
    }

    implementation {
        isTransitive = false
        // jackson2 has new group com.fasterxml.jackson. Old one sometimes needed by test libraries
        exclude(group = "org.codehaus.jackson")
        // hibernate-validator moved to group org.hibernate.validator
        exclude(group = "org.hibernate", module = "hibernate-validator")
    }


    all {
        // using mockito2
        exclude(module = "mockito-all")
        // no log4j, we use logback
        exclude(module = "log4j")
        exclude(module = "slf4j-log4j12")

        // using jcl-over-slf4j
        exclude(module = "commons-logging")
        // using hamcrest-all instead
        exclude(module = "hamcrest-core")
        exclude(module = "hamcrest-library")
    }
}

setupResolutionStrategy(listOf(
        configurations.implementation,
        configurations.api,
        configurations.compileOnly,
        configurations.runtimeOnly,
        configurations.runtimeClasspath,
        configurations.testImplementation,
        configurations.testCompileOnly,
        configurations.testRuntimeOnly,
        configurations.testRuntimeClasspath,

))

fun setupResolutionStrategy(configurations: List<NamedDomainObjectProvider<Configuration>>) {
    configurations.forEach { configuration ->
        configuration {
            resolutionStrategy {
                // dependencyUpdates tasks cannot deal with forced versions
                if (!gradle.startParameter.taskNames.contains("dependencyUpdates")) {
                    // ignore version conflicts in test dependencies, let gradle autohandle
                    // fail eagerly on version conflict (includes transitive dependencies)
                    // e.g. multiple different versions of the same dependency (group and name are equal)
                    failOnVersionConflict()

                    // cache dynamic versions for 10 minutes (these are versions like '2.4.+')
                    cacheDynamicVersionsFor(10 * 60, "seconds")
                    // don't cache changing modules at all
                    cacheChangingModulesFor(0, "seconds")
               }
            }
        }
    }
}
