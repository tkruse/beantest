package com.beantest.autovalue;

import com.beantest.domain.Vehicle;

/**
 */
public abstract class AVVehicle implements Vehicle {

    @Override
    public abstract String getBrand();
}
