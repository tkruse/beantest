package com.beantest.autovalue;

import com.beantest.domain.Wheel;
import com.google.auto.value.AutoValue;

/**
 */
@AutoValue
public abstract class AVWheel implements Wheel {
    @Override
    public abstract double getDiameter();

    public static AVWheel.Builder builder() {
        return new AutoValue_AVWheel.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract AVWheel.Builder setDiameter(double value);
        public abstract AVWheel autobuild();
    }
}
