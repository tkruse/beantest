package com.beantest.autovalue;

import com.beantest.domain.Bus;
import com.beantest.domain.Seat;
import com.beantest.domain.Wheel;
import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 */
@AutoValue
public abstract class AVBus extends AVVehicle implements Bus {

    @Nonnull
    @Size(min = 5)
    @Override
    public abstract String getName();

    @CheckForNull
    @Nullable
    @Override
    public abstract String getDriverName();

    @Override
    public abstract ImmutableList<Wheel> getWheels();

    @Override
    public abstract Map<String, Seat> getReservation();

    public static Builder builder() {
        return new AutoValue_AVBus.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setName(String value);
        public abstract Builder setDriverName(String name);
        public abstract Builder setBrand(String value);

        public abstract Builder setReservation(Map<String, Seat> name);

        public abstract Builder setWheels(List<Wheel> name);
        // method to have an addWheel method in Builder
        protected abstract ImmutableList.Builder<Wheel> wheelsBuilder();
        public Builder addWheel(Wheel wheel) {
            wheelsBuilder().add(wheel);
            return this;
        }

        public abstract Optional<String> getDriverName();
        public abstract Optional<ImmutableList<Wheel>> getWheels();
        public abstract Optional<Map<String, Seat>> getReservation();

        public abstract AVBus autobuild();
        public AVBus build() {
            if (!getReservation().isPresent()) {
                setReservation(ImmutableMap.of()); // should use immutable collection
            }
            return autobuild();
        }
    }

}
