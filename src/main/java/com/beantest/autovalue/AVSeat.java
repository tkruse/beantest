package com.beantest.autovalue;

import com.beantest.domain.Seat;
import com.google.auto.value.AutoValue;

/**
 */
@AutoValue
public abstract class AVSeat implements Seat {

    @Override
    public abstract String getPassengerName();

}
