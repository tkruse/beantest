package com.beantest.mapstruct;

import com.beantest.autovalue.AVBus;
import com.beantest.freebuilder.FBBus;
import com.beantest.immutables.ImmuBus;
import com.beantest.lombok.LombokBus;
import org.mapstruct.Mapper;

@Mapper
public interface MapStructMapper {


    //AVBus lombokToAutoValue(LombokBus bus);
    //FBBus autoValueToFreebuilder(AVBus bus);
    //ImmutableImmuBus freebuilderToImmutables(FBBus bus);
    //ImmutableImmuBus lombokToImmutables(LombokBus bus);

    LombokBus immutableToLombok(ImmuBus bus);
    LombokBus freebuilderToLombok(FBBus bus);
    LombokBus autoValueToLombok(AVBus bus);

}
