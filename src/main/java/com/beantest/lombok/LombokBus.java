package com.beantest.lombok;

import com.beantest.domain.Bus;
import com.beantest.domain.Seat;
import com.beantest.domain.Wheel;
import lombok.*;

import javax.annotation.Nonnull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("PMD.MissingStaticMethodInNonInstantiatableClass")
public class LombokBus extends LombokVehicle implements Bus {

    @Nonnull
    @Size(min = 5)
    private String name;

    @Nonnull
    private String driverName;

    @Nonnull
    private List<Wheel> wheels;

    private Map<String, Seat> reservation;

    @Builder // Need this constructor to get inheritance of builder
    private LombokBus(String brand, @NonNull String name, String driverName, List<Wheel> wheels, Map<String, Seat> reservation) {
        super(brand);
        this.name = name;
        this.driverName = driverName;
        this.wheels = wheels;
        this.reservation = reservation;
    }
}
