package com.beantest.autovalue;


public class Starter {

    public static void main(final String[] args) {
        // OK: Findbug warnings about NullPointerDereference
        //final LombokBus bus3 = LombokBus.builder().setName("CityBus").build();
        //System.out.println(bus3.driverName().length());
    }
}
