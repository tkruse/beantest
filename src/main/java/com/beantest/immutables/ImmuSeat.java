package com.beantest.immutables;

import com.beantest.domain.Seat;
import org.immutables.value.Value;

/**
 */
@Value.Modifiable
public abstract class ImmuSeat implements Seat {

    @Override
    public abstract String getPassengerName();

}
