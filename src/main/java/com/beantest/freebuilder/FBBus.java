package com.beantest.freebuilder;

import com.beantest.domain.Bus;
import com.beantest.domain.Seat;
import com.beantest.domain.Wheel;
import org.inferred.freebuilder.FreeBuilder;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 */
@FreeBuilder
public abstract class FBBus extends FBVehicle implements Bus {

    @Nonnull
    @Size(min = 5)
    @Override
    public abstract String getName();

    @CheckForNull // freebuilder ignores CheckForNull
    @Nullable
    @Override
    public abstract String getDriverName();

    @Override
    public abstract List<Wheel> getWheels();

    @Override
    public abstract Map<String, Seat> getReservation();


    public static class Builder extends FBBus_Builder {
    }

}
