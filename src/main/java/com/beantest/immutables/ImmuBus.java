package com.beantest.immutables;

import com.beantest.domain.Bus;
import com.beantest.domain.Seat;
import com.beantest.domain.Wheel;
import org.immutables.value.Value;

import javax.annotation.Nonnull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 */
@Value.Immutable
public abstract class ImmuBus extends ImmuVehicle implements Bus {

    @Nonnull
    @Size(min = 5)
    @Override
    public abstract String getName();

    // Not OK, Immutable LombokBus implementation raises Findbugs warning for other()
    //@CheckForNull
    //@Nullable // when using both, Not OK, no more warnings from findbugs
    @Override
    public abstract String getDriverName();

    @Override
    public abstract List<Wheel> getWheels();

    @Override
    public abstract Map<String, Seat> getReservation();



}
