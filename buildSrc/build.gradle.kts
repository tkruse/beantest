plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    // default dependencies
    implementation(gradleApi())

    implementation("org.inferred.processors:org.inferred.processors.gradle.plugin:3.6.0")
    implementation("com.github.ben-manes:gradle-versions-plugin:0.41.0")
    implementation("com.diffplug.spotless:spotless-plugin-gradle:6.2.0")
    implementation("com.github.spotbugs.snom:spotbugs-gradle-plugin:5.0.4")



    testImplementation("junit:junit:4.13.2")
}

tasks.test {
    useJUnit()
    testLogging {
        showStandardStreams = true
        setExceptionFormat("full") // default is "short"
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

// choosing a flat layout
sourceSets {
    main {
        java {
            setSrcDirs(listOf("src"))
        }
        kotlin {

        }
    }
    test {
        java {
            setSrcDirs(listOf("test"))
        }
    }
}
