package com.beantest.domain;

/**
 */
public interface Seat {

    String getPassengerName();

}
