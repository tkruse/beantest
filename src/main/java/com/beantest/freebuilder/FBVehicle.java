package com.beantest.freebuilder;


import com.beantest.domain.Vehicle;
import org.inferred.freebuilder.FreeBuilder;

/**
 */
@FreeBuilder
public abstract class FBVehicle implements Vehicle {

    @Override
    public abstract String getBrand();

    public static class Builder extends FBVehicle_Builder { }
}
