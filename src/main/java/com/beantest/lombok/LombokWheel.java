package com.beantest.lombok;

import com.beantest.domain.Wheel;
import lombok.*;

/**
 */
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Data
@Builder
public class LombokWheel implements Wheel {
    private double diameter;
}
