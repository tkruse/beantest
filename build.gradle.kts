plugins {
    `java-library-convention`
}

repositories {
    // Use JCenter for resolving dependencies.
    mavenCentral()
}

// define properties of all subprojects
group = "com.beantest"
description = "compare bean utils"



dependencies {

    compileOnly("org.inferred:freebuilder:${Versions.freebuilder}")
    annotationProcessor("org.inferred:freebuilder:${Versions.freebuilder}")

    annotationProcessor("com.google.auto.value:auto-value:${Versions.autovalue}")
    compileOnly("com.google.auto.value:auto-value-annotations:${Versions.autovalue}")

    compileOnly("org.projectlombok:lombok:${Versions.lombok}")
    annotationProcessor("org.projectlombok:lombok:${Versions.lombok}")
    testCompileOnly("org.projectlombok:lombok:${Versions.lombok}")
    testAnnotationProcessor("org.projectlombok:lombok:${Versions.lombok}")

    // must come after lombok
    compileOnly(Libraries.mapstruct)
    testImplementation(Libraries.mapstruct)
    annotationProcessor(Libraries.mapstruct_processor)

    implementation("org.immutables:value:${Versions.immutables}") // for annotations
    annotationProcessor("org.immutables:value:${Versions.immutables}") // for annotations
    annotationProcessor("org.immutables:builder:${Versions.immutables}") // for annotations
    annotationProcessor("org.immutables:gson:${Versions.immutables}") // for annotations

    /* REST endpoint input validation annotations */
    implementation("javax.validation:validation-api:${Versions.javax_validation}")
    runtimeOnly("org.hibernate:hibernate-validator:${Versions.hibernate_valid}")
}
