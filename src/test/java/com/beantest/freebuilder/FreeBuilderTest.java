package com.beantest.freebuilder;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class FreeBuilderTest {

    @Test
    public void testBuilder() {
        assertThat(new FBBus.Builder().setDriverName("John").setBrand("Ford").setName("schoolbus").build()).isNotNull();
        assertThatThrownBy(() -> new FBBus.Builder().setName(null).build()).isInstanceOf(NullPointerException.class);
    }
}
