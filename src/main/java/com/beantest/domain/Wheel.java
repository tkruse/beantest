package com.beantest.domain;


/**
 */
public interface Wheel {

    double getDiameter();
}
