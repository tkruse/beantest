package com.beantest.freebuilder;

import com.beantest.domain.Wheel;
import org.inferred.freebuilder.FreeBuilder;

/**
 */
@FreeBuilder
public abstract class FBWheel implements Wheel {
    @Override
    public abstract double getDiameter();

    public static class Builder extends FBWheel_Builder { }
}
