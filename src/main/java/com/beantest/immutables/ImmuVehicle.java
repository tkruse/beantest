package com.beantest.immutables;

import com.beantest.domain.Vehicle;
import org.immutables.value.Value;

/**
 */
@Value.Modifiable
public abstract class ImmuVehicle implements Vehicle {

    @Override
    public abstract String getBrand();
}
