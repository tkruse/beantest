import com.github.spotbugs.snom.SpotBugsTask
import java.nio.file.Paths

plugins {
    `java-library`
    id("com.github.spotbugs")
}


var lintersDir = "${project.rootDir}/buildSrc/linters"

// use with -Pxmlreports for reports in xml that Jenkins can read and check
val xmlreports = project.hasProperty("xmlreports")
// use with -PuseSpotbugs, very slow so disabled by default
val useSpotbugs = project.hasProperty("useSpotbugs")

dependencies {
    spotbugs("com.github.spotbugs:spotbugs:${ToolVersions.spotbugs}")
}

spotbugs {
    effort.set(com.github.spotbugs.snom.Effort.MAX)
    // low means all issues are reported
    reportLevel.set(com.github.spotbugs.snom.Confidence.LOW)
    ignoreFailures.set(false)
    excludeFilter.set(file(Paths.get(lintersDir, "spotbugs/excludeFilter.xml")))
    // onlyAnalyze(listOf<>(sourceSets.main, sourceSets.test))
}

tasks.withType<SpotBugsTask> {
    mustRunAfter(tasks.test) // care about test results first
    enabled = useSpotbugs
    if (xmlreports) {
        reports.create("xml") {
            required.set(true)
        }
    }
}
