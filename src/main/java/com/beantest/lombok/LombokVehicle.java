package com.beantest.lombok;

import com.beantest.domain.Vehicle;
import lombok.*;

/**
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LombokVehicle implements Vehicle {
    private String brand;
}
