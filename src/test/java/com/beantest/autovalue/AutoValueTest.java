package com.beantest.autovalue;

import com.beantest.lombok.LombokWheel;
import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AutoValueTest {

    @Test
    public void testBuilder() {
        assertThat(AVBus.builder()
                .setDriverName("John")
                .setBrand("Ford")
                .setName("schoolbus")
                .setWheels(Collections.singletonList(AVWheel.builder().setDiameter(11.0).autobuild()))
                .addWheel(AVWheel.builder().setDiameter(22.0).autobuild())
                .build())
                .isNotNull()
                .satisfies(bus -> {
                    assertThat(bus.getDriverName()).isEqualTo("John");
                    assertThat(bus.getBrand()).isEqualTo("Ford");
                    assertThat(bus.getName()).isEqualTo("schoolbus");
                    assertThat(bus.getWheels()).hasSize(2);
                    assertThat(bus.getWheels().get(0)).usingRecursiveComparison().isEqualTo(
                            LombokWheel.builder().diameter(11.0).build());
                    assertThat(bus.getWheels().get(1)).usingRecursiveComparison().isEqualTo(
                            LombokWheel.builder().diameter(22.0).build());
                });

        assertThatThrownBy(() -> AVBus.builder().setName(null).build()).isInstanceOf(NullPointerException.class);
    }
}
