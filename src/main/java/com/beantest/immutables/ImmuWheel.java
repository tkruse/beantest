package com.beantest.immutables;

import com.beantest.domain.Wheel;
import org.immutables.value.Value;

/**
 */
@Value.Immutable
public abstract class ImmuWheel implements Wheel {
    @Override
    public abstract double getDiameter();
}
