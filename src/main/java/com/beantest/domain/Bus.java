package com.beantest.domain;

import java.util.List;
import java.util.Map;

public interface Bus {

    String getName();

    String getDriverName();

    List<Wheel> getWheels();

    Map<String, Seat> getReservation();

}
