package com.beantest.immutables;


public class Starter {

    public static void main(final String[] args) {
        // OK: NPE in constructor
        // final LombokBus bus1 = ImmutableBus.builder().name(null).build();
        // OK: NPE in constructor
        // Lfinal ombokBus bus2 = LombokBus.builder().driverName("John").build();
        // final LombokBus bus3 = ImmutableBus.builder().name("CityBus").build();
        // OK: Findbug warnings about NullPointerDereference
        //System.out.println(bus3.driverName().length());
    }
}
